import { h, r as registerInstance } from './chunk-a4031aec.js';
var AppRouter = function () { return (h("stencil-router", null, h("stencil-route-switch", { scrollTopOffset: 0 }, h("stencil-route", { url: "/", component: "app-home", exact: true }), h("stencil-route", { url: "/profile/:name", component: "app-profile" }), h("stencil-route", { url: "/form", component: "app-form", exact: true })))); };
var AppRoot = /** @class */ (function () {
    function AppRoot(hostRef) {
        registerInstance(this, hostRef);
    }
    AppRoot.prototype.render = function () {
        return (h("div", null, h("main", null, h(AppRouter, null))));
    };
    Object.defineProperty(AppRoot, "style", {
        get: function () { return "header{background:#5851ff;height:56px;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-webkit-box-shadow:0 2px 5px 0 rgba(0,0,0,.26);box-shadow:0 2px 5px 0 rgba(0,0,0,.26)}h1,header{color:#fff}h1{font-size:1.4rem;font-weight:500;padding:0 12px}"; },
        enumerable: true,
        configurable: true
    });
    return AppRoot;
}());
export { AppRoot as app_root };
