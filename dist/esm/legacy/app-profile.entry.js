import { r as registerInstance, h } from './chunk-a4031aec.js';
var normalize = function (name) { return name
    ? "" + name.substr(0, 1).toUpperCase() + name.substr(1).toLowerCase()
    : ""; };
var AppProfile = /** @class */ (function () {
    function AppProfile(hostRef) {
        registerInstance(this, hostRef);
    }
    AppProfile.prototype.render = function () {
        var match = this.match;
        if (match && match.params.name) {
            return (h("div", { class: "app-profile" }, h("p", null, "Hello! My name is ", normalize(match.params.name), ". My name was passed in through a route param!")));
        }
    };
    Object.defineProperty(AppProfile, "style", {
        get: function () { return ".app-profile{padding:10px}"; },
        enumerable: true,
        configurable: true
    });
    return AppProfile;
}());
export { AppProfile as app_profile };
