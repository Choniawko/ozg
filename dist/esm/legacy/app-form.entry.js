import { r as registerInstance, h } from './chunk-a4031aec.js';
var Form = /** @class */ (function () {
    function Form(hostRef) {
        registerInstance(this, hostRef);
        this.stateForm = { username: "", email: "" };
    }
    Form.prototype.handleChange = function (e) {
        var _a;
        this.stateForm = Object.assign({}, this.stateForm, (_a = {}, _a[e.target.name] = e.target.value, _a));
    };
    Form.prototype.render = function () {
        return (h("form", null, h("div", null, h("input", { type: "input", name: "username", value: this.stateForm.username, onInput: this.handleChange })), h("div", null, h("input", { type: "input", name: "email", value: this.stateForm.email, onInput: this.handleChange }))));
    };
    return Form;
}());
export { Form as app_form };
