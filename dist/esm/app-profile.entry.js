import { r as registerInstance, h } from './chunk-a4031aec.js';

const normalize = (name) => name
    ? `${name.substr(0, 1).toUpperCase()}${name.substr(1).toLowerCase()}`
    : "";

class AppProfile {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        const { match } = this;
        if (match && match.params.name) {
            return (h("div", { class: "app-profile" }, h("p", null, "Hello! My name is ", normalize(match.params.name), ". My name was passed in through a route param!")));
        }
    }
    static get style() { return ".app-profile{padding:10px}"; }
}

export { AppProfile as app_profile };
