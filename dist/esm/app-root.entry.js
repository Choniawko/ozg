import { h, r as registerInstance } from './chunk-a4031aec.js';

const AppRouter = () => (h("stencil-router", null,
    h("stencil-route-switch", { scrollTopOffset: 0 },
        h("stencil-route", { url: "/", component: "app-home", exact: true }),
        h("stencil-route", { url: "/profile/:name", component: "app-profile" }),
        h("stencil-route", { url: "/form", component: "app-form", exact: true }))));

class AppRoot {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("div", null, h("main", null, h(AppRouter, null))));
    }
    static get style() { return "header{background:#5851ff;height:56px;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-webkit-box-shadow:0 2px 5px 0 rgba(0,0,0,.26);box-shadow:0 2px 5px 0 rgba(0,0,0,.26)}h1,header{color:#fff}h1{font-size:1.4rem;font-weight:500;padding:0 12px}"; }
}

export { AppRoot as app_root };
