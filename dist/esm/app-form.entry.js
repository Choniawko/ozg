import { r as registerInstance, h } from './chunk-a4031aec.js';

class Form {
    constructor(hostRef) {
        registerInstance(this, hostRef);
        this.stateForm = { username: "", email: "" };
    }
    handleChange(e) {
        this.stateForm = Object.assign({}, this.stateForm, { [e.target.name]: e.target.value });
    }
    render() {
        return (h("form", null, h("div", null, h("input", { type: "input", name: "username", value: this.stateForm.username, onInput: this.handleChange })), h("div", null, h("input", { type: "input", name: "email", value: this.stateForm.email, onInput: this.handleChange }))));
    }
}

export { Form as app_form };
