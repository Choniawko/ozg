'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const __chunk_1 = require('./chunk-fa101c45.js');

const AppRouter = () => (__chunk_1.h("stencil-router", null,
    __chunk_1.h("stencil-route-switch", { scrollTopOffset: 0 },
        __chunk_1.h("stencil-route", { url: "/", component: "app-home", exact: true }),
        __chunk_1.h("stencil-route", { url: "/profile/:name", component: "app-profile" }),
        __chunk_1.h("stencil-route", { url: "/form", component: "app-form", exact: true }))));

class AppRoot {
    constructor(hostRef) {
        __chunk_1.registerInstance(this, hostRef);
    }
    render() {
        return (__chunk_1.h("div", null, __chunk_1.h("main", null, __chunk_1.h(AppRouter, null))));
    }
    static get style() { return "header{background:#5851ff;height:56px;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-webkit-box-shadow:0 2px 5px 0 rgba(0,0,0,.26);box-shadow:0 2px 5px 0 rgba(0,0,0,.26)}h1,header{color:#fff}h1{font-size:1.4rem;font-weight:500;padding:0 12px}"; }
}

exports.app_root = AppRoot;
