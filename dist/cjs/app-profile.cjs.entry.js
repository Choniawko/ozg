'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const __chunk_1 = require('./chunk-fa101c45.js');

const normalize = (name) => name
    ? `${name.substr(0, 1).toUpperCase()}${name.substr(1).toLowerCase()}`
    : "";

class AppProfile {
    constructor(hostRef) {
        __chunk_1.registerInstance(this, hostRef);
    }
    render() {
        const { match } = this;
        if (match && match.params.name) {
            return (__chunk_1.h("div", { class: "app-profile" }, __chunk_1.h("p", null, "Hello! My name is ", normalize(match.params.name), ". My name was passed in through a route param!")));
        }
    }
    static get style() { return ".app-profile{padding:10px}"; }
}

exports.app_profile = AppProfile;
