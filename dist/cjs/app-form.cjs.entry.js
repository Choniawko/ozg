'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const __chunk_1 = require('./chunk-fa101c45.js');

class Form {
    constructor(hostRef) {
        __chunk_1.registerInstance(this, hostRef);
        this.stateForm = { username: "", email: "" };
    }
    handleChange(e) {
        this.stateForm = Object.assign({}, this.stateForm, { [e.target.name]: e.target.value });
    }
    render() {
        return (__chunk_1.h("form", null, __chunk_1.h("div", null, __chunk_1.h("input", { type: "input", name: "username", value: this.stateForm.username, onInput: this.handleChange })), __chunk_1.h("div", null, __chunk_1.h("input", { type: "input", name: "email", value: this.stateForm.email, onInput: this.handleChange }))));
    }
}

exports.app_form = Form;
