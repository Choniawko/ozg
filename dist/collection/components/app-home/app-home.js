import { h } from "@stencil/core";
export class AppHome {
    render() {
        return (h("div", { class: "app-home" },
            h("p", null,
                "Welcome to the Stencil App Starter. You can use this starter to build entire apps all with web components using Stencil! Check out our docs on ",
                h("a", { href: "https://stenciljs.com" }, "stenciljs.com"),
                " to get started."),
            h("stencil-route-link", { url: "/form" },
                h("button", null, "Form page"))));
    }
    static get is() { return "app-home"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["app-home.css"]
    }; }
    static get styleUrls() { return {
        "$": ["app-home.css"]
    }; }
}
