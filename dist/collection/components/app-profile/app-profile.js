import { h } from "@stencil/core";
import { normalize } from "./utils";
export class AppProfile {
    render() {
        const { match } = this;
        if (match && match.params.name) {
            return (h("div", { class: "app-profile" },
                h("p", null,
                    "Hello! My name is ",
                    normalize(match.params.name),
                    ". My name was passed in through a route param!")));
        }
    }
    static get is() { return "app-profile"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["app-profile.css"]
    }; }
    static get styleUrls() { return {
        "$": ["app-profile.css"]
    }; }
    static get properties() { return {
        "match": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "MatchResults",
                "resolved": "MatchResults",
                "references": {
                    "MatchResults": {
                        "location": "import",
                        "path": "@stencil/router"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        }
    }; }
}
