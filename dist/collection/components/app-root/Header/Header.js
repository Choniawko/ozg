import { h } from "@stencil/core";
export const Header = () => (h("header", null,
    h("h1", null, "Stencil App Starter")));
