import { h } from "@stencil/core";
export const AppRouter = () => (h("stencil-router", null,
    h("stencil-route-switch", { scrollTopOffset: 0 },
        h("stencil-route", { url: "/", component: "app-home", exact: true }),
        h("stencil-route", { url: "/profile/:name", component: "app-profile" }),
        h("stencil-route", { url: "/form", component: "app-form", exact: true }))));
