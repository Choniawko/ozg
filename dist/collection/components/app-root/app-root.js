import { h } from "@stencil/core";
import { AppRouter } from "./AppRouter";
export class AppRoot {
    render() {
        return (h("div", null,
            h("main", null,
                h(AppRouter, null))));
    }
    static get is() { return "app-root"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["app-root.css"]
    }; }
    static get styleUrls() { return {
        "$": ["app-root.css"]
    }; }
}
