import { h } from "@stencil/core";
export class Form {
    constructor() {
        this.stateForm = { username: "", email: "" };
    }
    handleChange(e) {
        this.stateForm = Object.assign({}, this.stateForm, { [e.target.name]: e.target.value });
    }
    render() {
        return (h("form", null,
            h("div", null,
                h("input", { type: "input", name: "username", value: this.stateForm.username, onInput: this.handleChange })),
            h("div", null,
                h("input", { type: "input", name: "email", value: this.stateForm.email, onInput: this.handleChange }))));
    }
    static get is() { return "app-form"; }
    static get states() { return {
        "stateForm": {}
    }; }
}
