import { MatchResults } from "@stencil/router";
export declare class AppProfile {
    match: MatchResults;
    render(): any;
}
