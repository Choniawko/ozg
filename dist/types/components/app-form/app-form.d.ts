interface IForm {
    username: string;
    email: string;
}
export declare class Form {
    stateForm: IForm;
    handleChange(e: any): void;
    render(): any;
}
export {};
